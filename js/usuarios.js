function mostrarDatos(){
    $.ajax({
        type: 'POST',
        url: 'util/usuarios/query.php',
        dataType: 'json',
        beforeSend: function(){
            $("#cntTabla").hide();
            $("#cargando").html('<img src="img/system/91.gif" height="70" width="70">');
        },
        error: function(request, status, error){
            alert(request.responseText);
        },
        success: function(respuesta){
            $("#cargando").html('');
            $("#cntTabla").show();

            var myData= respuesta.data;

            for(var i=0; i<myData.length; i++){
                myData[i].row=i+1;
            }

            //la data que se trae del query.php respectivo de los usuarios se pone en la tabla
            //se hace una matriz para poder ordenar segun las columnas de la tabla

            
            $("#tablaUsuarios").dataTable({
                destroy: true,
                data: myData,
                columns: [
                    {"data":'row'},
                    {"data":'btn_gestion', className: "text-center"},
                    {"data":'usuario', className: "text-center"},
                    {"data":'nombre', className: "text-center"},
                    {"data":'tipo_usuario', className: "text-center"},
                    {"data":'email', className: "text-center"},
                ],
                "columnDefs":[
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": [0]
                    }
                ],
                "order": [[0, 'asc']], //ascendente o descendente
                "language":{"url":"lib/js/DataTables/DataTables-1.10.20/Spanish.json"},
                "pagingType": "full_numbers" //para colocar el first y el last

            });

            // t.on( 'order.dt search.dt', function () {
            //     t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            //         cell.innerHTML = i+1;
            //     } );
            // } ).draw();

        },
    });
}

function limpiarFormUsuario(){
    $("#formUsuario").trigger("reset");
    $("#usuario").prop('disabled', false);
}


$(document).ready(function (){

    $("#consultar").click(
        function(){
            limpiarFormUsuario();
            mostrarDatos();
        }
    );

    $("#limpiarFormUsuarios").click(
        function() {
            limpiarFormUsuario();
        }
    );

    //para que al dar click en el sub-boton creado dentro de la tabla, lleve a la pestania de gestion
    $("#tablaUsuarios").on("click", ".gestion_update", function(event){
        var tableInt= $("#tablaUsuarios").DataTable();
        var datos= tableInt.row($(this).closest('tr')).data();

        $("#usuario").prop('disabled', true);
        $("#usuario").val(datos['usuario']);
        $("#Nombre").val(datos['nombre']);
        $("#email").val(datos['email']);
        $('#tipoUsuario option[value="' + datos['tipo_usuario'] + '"]').prop('selected', true);

        $('.nav-tabs a[href="#menu1"]').tab('show');

    });


    //funcion para enviar datos mediante ajax
    $("#formUsuario").submit(
        function() {
            var existe= ($("#usuario").is(":disabled") ? 1 : 0);
            var usuario= $('#usuario').val().toLowerCase();
            var nombre= $('#Nombre').val();
            var email= $('#email').val();
            var tipo_usuario= $('#tipoUsuario option:selected').val();

            //para eliminar todos los espacios en blanco del usuario
            var usuario_Send= usuario.replace(/\s/g, "");
            $('#usuario').val(usuario_Send);
            $.ajax({
                async: false,
                type: 'POST',
                url: 'util/usuarios/gestion.php',
                dataType: 'json',
                data: {existe: existe, usuario: usuario_Send, nombre: nombre, email: email, tipo_usuario:tipo_usuario},
                error: function(request, status, error){
                    //alert(request.responseText);
                    console.log(request.responseText);
                },

                success: function(respuesta) {
                    switch (respuesta.estado) {
                        case 1:
                            $('#myModalSuccessBody').html(respuesta.mensaje);
                            $('#myModalSuccess').modal('show');
                            $('.nav-tabs a[href="#home"]').tab('show');
                            mostrarDatos();
                            limpiarFormUsuario();
                            break;
                        case 2:
                            $('#myModalWarningBody').html(respuesta.mensaje);
                            $('#myModalWarning').modal('show');
                            break;
                        default:
                            alert(respuesta);
                            break;
                    }
                },
                complete: function(){
                    $('#btn_submit').prop("disabled", false);
                    $('#btn_submit').html("Acceder");
                }
                }
            );


            return false;

        }
    );

});