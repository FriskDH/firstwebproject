$(document).ready(function (){
    //$('#myModalWarning').modal('show');
    //activar el envío de datos
    $('#formLogin').submit(function () {
        //obtener los datos
        var usuario = $("#usuario").val().toLowerCase();
        var clave = $("#clave").val();
        //implementación de ajax
        $.ajax({
            async: true,
            type: 'POST',
            //en la url no se sube de carpeta "../util..." porque js se renderiza en la raíz
            url: 'util/login/login.php',
            data: {
                usuario: usuario,
                clave: clave
            },
            //esta es la data que se recibe desde el servidor
            dataType: 'json',
            beforeSend: function(){
                //hacer que el botón de ingresar quede inhabilitado una vez se presiona
                $('#btnSubmit').html(':D');
                //con prop se entra a las propiedades
                $('#btnSubmit').prop('disabled', true);
            },

            error: function(request, status, error){
                alert(request.responseText);
            },
            success: function(respuesta){
                switch (respuesta.estado) {
                    case 1:
                        document.location = '';
                        console.log('ahuevos si funciona');
                        break;
                    case 2:
                        //mostrar mensaje de error en caso de que el estado (en el login/login.php) sea 2
                        $('#myModalWarningBody').html(respuesta.mensaje);
                        $('#myModalWarning').modal('show');
                        $('#clave').val('');
                        break;
                    default:
                        alert("Se ha producido un error");
                        break;
                }
            },

            complete: function(){
                $('#btnSubmit').prop('disabled', false);
                $('#btnSubmit').html('Ingresar');
            }
        });

        return false;
    });
});