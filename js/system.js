function logoutSession(){
    document.location = 'util/system/logout.php';
}

$(document).ready(function () {
    var parametro_timeout = $('#parametro_timout').val();
    setInterval(logoutSession, (parseInt(parametro_timeout)*60000));
    if(typeof Highcharts !== 'undefined'){
        Highcharts.setOptions({
           lang: {
               printChart: 'Imprimir Grafico',
               downloadCSV: 'Descargar CSV',
               downloadJPEG: 'Descargar JPEG',
               downloadPDF: 'Descargar PDF',
               downloadPNG: 'Descargar PNG',
               downloadSVG: 'Descargar SVG',
               downloadXLS: 'Descargar XLS',
               viewAsDataTable: 'Ver como tabla de datos',
               viewData: 'Ver tabla de datos',
               viewFullscreen: 'Ver en pantalla completa',
               openInCloud: 'Ver en la nube'
           }
        });
    }
});