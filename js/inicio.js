// function mostrarGraficoInicio(){
//     $.ajax({
//         //async: true,
//         type: 'POST',
//         //en la url no se sube de carpeta "../util..." porque js se renderiza en la raíz
//         url: 'util/inicio/query.php',
//         // data: {
//         //     usuario: usuario,
//         //     clave: clave
//         // },
//         //esta es la data que se recibe desde el servidor
//         dataType: 'json',
//         //beforeSend: function(){},
//         error: function(request, status, error){
//             console.log(error.toString());
//             alert(request.toString());
//         },
//         success: function(respuesta){
//             switch (respuesta.estado) {
//                 case 1:
//                     document.location = '';
//                     console.log('respuesta');
//                     break;
//                 case 2:
//                     //mostrar mensaje de error en caso de que el estado (en el login/login.php) sea 2
//                     $('#myModalWarningBody').html(respuesta.mensaje);
//                     $('#myModalWarning').modal('show');
//                     break;
//                 default:
//                     alert("Se ha producido un error");
//                     break;
//             }
//         },
//         //complete: function(){}
//     });
// }

$(document).ready(function () {
    Highcharts.chart('container', {

        chart: {
            scrollablePlotArea: {
                minWidth: 700
            }
        },

        data: {
            csvURL: 'https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/analytics.csv',
            beforeParse: function (csv) {
                return csv.replace(/\n\n/g, '\n');
            }
        },

        title: {
            text: 'Daily sessions at www.highcharts.com'
        },

        subtitle: {
            text: 'Source: Google Analytics'
        },

        xAxis: {
            tickInterval: 7 * 24 * 3600 * 1000, // one week
            tickWidth: 0,
            gridLineWidth: 1,
            labels: {
                align: 'left',
                x: 3,
                y: -3
            }
        },

        yAxis: [{ // left y axis
            title: {
                text: null
            },
            labels: {
                align: 'left',
                x: 3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        }, { // right y axis
            linkedTo: 0,
            gridLineWidth: 0,
            opposite: true,
            title: {
                text: null
            },
            labels: {
                align: 'right',
                x: -3,
                y: 16,
                format: '{value:.,0f}'
            },
            showFirstLabel: false
        }],

        legend: {
            align: 'left',
            verticalAlign: 'top',
            borderWidth: 0
        },

        tooltip: {
            shared: true,
            crosshairs: true
        },

        plotOptions: {
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function (e) {
                            hs.htmlExpand(null, {
                                pageOrigin: {
                                    x: e.pageX || e.clientX,
                                    y: e.pageY || e.clientY
                                },
                                headingText: this.series.name,
                                maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) + ':<br/> ' +
                                    this.y + ' sessions',
                                width: 200
                            });
                        }
                    }
                },
                marker: {
                    lineWidth: 1
                }
            }
        },

        series: [{
            name: 'All sessions',
            lineWidth: 4,
            marker: {
                radius: 4
            }
        }, {
            name: 'New users'
        }]
    });

});