function cargarOptionUsuarios(){
    $.ajax({
        type: 'POST',
        url: 'util/accesos/optionUsuarios.php',
        dataType: 'json',
        //antes de enviar datos se limpia el select de los usuarios
        beforeSend: function(){
            $('#usuarios').empty();
        },
        error: function(request, status, error){
            alert(request.responseText);
        },
        success: function(respuesta){
            switch (respuesta.estado) {
                case 1:
                    var opciones = respuesta.data;
                    if(opciones.length>0){
                        var opcSelect = '';
                        for(var f = 0; f<opciones.length; f++){
                            //concatenar el html
                            //con ++ se concatena
                            opcSelect += '<option ';
                            opcSelect += ' data-name="'+opciones[f]['nombre']+'"';
                            opcSelect += 'value="'+opciones[f]['usuario']+'">';
                            opcSelect += opciones[f]['usuario']+' - '+opciones[f]['nombre'];
                            opcSelect += '</option>';
                        }
                        $('#usuarios').html(opcSelect);
                        //implementar chosen
                        $('#usuarios').trigger("chosen:updated");
                    }
                    break;
                case 2:
                    $('#myModalWarningBody').html(respuesta.mensaje);
                    $('#myModalWarning').modal('show');
                    console.log(respuesta.mensaje);
                    break;
                default:
                    alert('Se ha producido un error :');
                    console.log('se ha producido un error');
                    break;

            }
        }
    });
}

function cargarMenu(){
    $.ajax({
        type: 'POST',
        url: 'util/accesos/queryMenu.php',
        dataType: 'json',
        //antes de enviar datos se limpia el select de los usuarios
        beforeSend: function(){
            $('#sys_menu').empty();
        },
        error: function(request, status, error){
            alert(request.responseText);
        },
        success: function(respuesta){
            switch (respuesta.estado) {
                case 1:
                    //console.log(respuesta);
                    var menu = respuesta.data;
                    var sys_menu = '<ul id="menuPermisos">';
                    for (var i = 0; i < menu.length; i++){
                        //aqui se despliegan los menus para poder checkearlos
                        sys_menu += '<li class="open">';
                        sys_menu += '<input type="checkbox" id="menu'+(i+1)+'  " value="'+menu[i]['idMenu']+'" class="menuSys">';
                        sys_menu += '<label for="menu'+(i+1)+'">'+menu[i]['nombre']+'</label>';

                        //aqui van los submenus
                        sys_menu += '<ul>';
                        var subMenu = menu[i]['subMenu'];

                        //ahora se itera el submenu
                        for (var j = 0; j<subMenu.length; j++){
                            sys_menu += '<li>';
                            sys_menu += '<input type="checkbox" id="subMenu'+(i+1)+''+(j+1)+'  " value="'+subMenu[j]['idMenu']+'" class="menuSys">';
                            sys_menu += '<label for="subMenu'+(i+1)+''+(j+1)+' ">'+subMenu[j]['nombre']+'</label>';

                            sys_menu += '</li>';
                        }

                        sys_menu += '</ul>';

                        sys_menu += '</li>';
                    }
                    sys_menu+='</ul>';
                    $('#sys_menu').html(sys_menu);
                    break;
                case 2:
                    $('#myModalWarningBody').html(respuesta.mensaje);
                    $('#myModalWarning').modal('show');
                    console.log(respuesta.mensaje);
                    break;
                default:
                    alert('Se ha producido un error :');
                    console.log('se ha producido un error');
                    break;

            }
        },
        complete: function(){
            checkear();
            $('#menuPermisos').treeview({
                animated: 'normal',
                collapsed: true,
                unique: false,
                persist: 'location'
            });
        }
    });
}

function checkear(){
    $('input[type="checkbox"]').change(function(e){
       var checked = $(this).prop("checked"),
           container = $(this).parent(),
           siblings = container.siblings();

       container.find('input[type="checkbox"]').prop({
           indeterminate: false,
           checked: checked
       });

       function checkSiblings(e1){
           var parent = e1.parent().parent(),
               all=true;

           e1.siblings().each(function() {
               return all = ($(this).children('input[type="checkbox"]').prop("checked")
               === checked);
           });
           if(all && checked){
               parent.children('input[type="checkbox"]').prop({
                   indeterminate: false,
                   checked: checked
               });
               checkSiblings(parent);
           }else if(all && !checked){
               parent.children('input[type="checkbox"]').prop("checked", checked);
               parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked')).length>0);
               checkSiblings(parent);
           }else{
               e1.parents('li').children('input[type="checkbox"]').prop({
                   indeterminate: true,
                   checked: true
               });
           }
       }
       checkSiblings(container);
    });
}

function limpiarFormPermisos(){
    $("#formGuardarPermisos").trigger("reset");
    $('input[type="checkbox"]').each(function(){
        $(this).prop({checked:false});
        $(this).prop({indeterminate:false});

    });
}

function mostrarMenuUsuario(usuario){
    $.ajax({
        async: false,
        type: 'POST',
        url: 'util/accesos/queryMenuUsuario.php',
        dataType: 'json',
        data: {usuario:usuario},
        //antes de enviar datos se limpia el select de los usuarios
        beforeSend: function(){
            $('input[type="checkbox"]').each(function(){
                $(this).prop({checked:false});
                $(this).prop({indeterminate:false});

            });
        },
        error: function(request, status, error){
            alert(request.responseText);
        },
        success: function(respuesta){
            switch (respuesta.estado) {
                case 1:
                    //aqui se iteran los valores retornados en la data para comprarlos con los menus
                    var menu = respuesta.data;

                    if(menu.length > 0){
                        $(".menuSys").each(function(){
                            var valor = this.value;

                            for(var i=0; i<menu.length; i++){
                                if(valor == menu[i]){
                                    $(this).prop({checked: true});
                                    break;
                                }
                            }
                        });
                    }else{
                        $('#myModalWarningBody').html("El usuario no tiene permisos asignados");
                        $('#myModalWarning').modal('show');
                    }
                    break;
                case 2:
                    $('#myModalWarningBody').html(respuesta.mensaje);
                    $('#myModalWarning').modal('show');
                    console.log(respuesta.mensaje);
                    break;
                default:
                    alert('Se ha producido un error :');
                    console.log('se ha producido un error');
                    break;

            }
        }
    });
}

$(document).ready(function () {
    cargarMenu();
    cargarOptionUsuarios();
    //luego de cargar los usuarios se implementa el chosen
    $('#usuarios').chosen();
   // $('.form-control-chosen').chosen({width: '100%'});

    $("#limpiarFormGuardarPermisos").click(function(){
        limpiarFormPermisos();
    });

     $("#formUsuarios").submit(function(){
         limpiarFormPermisos();
         var usuario = $("#usuarios option:selected").val();

         $("#usuario").val(usuario);
         $("#nombre").val($("#usuarios option:selected").data("name"));
         mostrarMenuUsuario(usuario);

         return false;
     });

    $("#formGuardarPermisos").submit(function(){
        var usuario=$("#usuario").val();
        if(usuario != ''){
            var menuCheckeado= [];
            var existeMenu= false;

            $('.menuSys').each(function(){
                existeMenu= true;
                var thisMenu=this;
                var valor = thisMenu.value;
                var checkeado= ( $(this).is(":checked") || $(this).prop("indeterminate") ? 1 : 0);
                if(checkeado == 1){
                    menuCheckeado.push(valor);
                }
            });
            // console.log(menuCheckeado);
            if(existeMenu){

                $.ajax({
                    async: false,
                    type: 'POST',
                    url: 'util/accesos/gestion.php',
                    dataType: 'json',
                    data: {
                        usuario:usuario,
                        menuCheckeado:menuCheckeado
                    },
                    // beforeSend: function(){
                    //     $('input[type="checkbox"]').each(function(){
                    //         $(this).prop({checked:false});
                    //         $(this).prop({indeterminate:false});
                    //
                    //     });
                    // },
                    error: function(request, status, error){
                        alert(request.responseText);
                    },
                    success: function(respuesta){
                        switch (respuesta.estado) {
                            case 1:
                                limpiarFormPermisos();
                                $('#myModalSuccessBody').html(respuesta.mensaje);
                                $('#myModalSuccess').modal('show');
                                break;
                            case 2:
                                $('#myModalWarningBody').html(respuesta.mensaje);
                                $('#myModalWarning').modal('show');
                                console.log(respuesta.mensaje);
                                break;
                            default:
                                alert('Se ha producido un error :');
                                console.log('se ha producido un error');
                                break;

                        }
                    }
                });


            }else{
                $('#myModalWarningBody').innerHTML("El menu no se ha cargado.");
                $('#myModalWarning').modal('show');
            }
        }else{
            $('#myModalWarningBody').innerHTML("Debe seleccionar un usuario.");
            $('#myModalWarning').modal('show');
        }

        return false;
    });



});