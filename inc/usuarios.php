<h1><?php echo $varAcceso['nombre']; ?></h1>
<ul class="nav nav-tabs nav-justified">
    <li class="nav-item"><a data-toggle="tab" href="#home" class="nav-link" ">Visualizar</a></li>
    <li class="nav-item"><a data-toggle="tab" href="#menu1" class="nav-link">Gestion</a></li>
</ul>

<div class="tab-content">
    <div id="home" class="row tab-pane fade in active">
        <div class="col-md-12">
            <div class="row page-header">
                <div class="col-md-12 form-group">
                    <br>
                    <br>
                    <br>
                    <button type="button" class="btn btn-block btn-primary" id="consultar">Consultar</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="cargando" class="text-center"></div>
                </div>
                <div id="cntTabla" class="col-md-12">
                    <div class="table-responsive">
                        <table id="tablaUsuarios" class="table cell-border stripe display" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Gestion</th>
                                <th>Usuario</th>
                                <th>Nombre</th>
                                <th>Tipo Usuario</th>
                                <th>e-Mail</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="menu1" class="row tab-pane fade">
        <br>
        <div class="col-md-12">
            <div class="alert alert-warning alert-dismissible">
                <ul>
                    <li>(*) Campos Obligatorios</li>
                    <li>Al crear un usuario, se establecera (<?php echo $parametros['claveusuario']; ?>)</li>
                </ul>
            </div>
            <form action="" id="formUsuario" role="form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="usuario">Usuario (*)</label>
                            <input type="text" id="usuario" maxlength="40" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre (*)</label>
                            <input type="text" id="Nombre" maxlength="40" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tipoUsuario">Tipo Usuario (*)</label>
                            <select name="" id="tipoUsuario" class="form-control">
                                <option value="ADMINISTRADOR" selected="selected">
                                    ADMINISTRADOR
                                </option>
                                <option value="VENDEDOR" selected="selected">
                                    VENDEDOR
                                </option>
                                <option value="CLIENTE" selected="selected">
                                    CLIENTE
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email">e-Mail (*)</label>
                            <input type="email" id="email" maxlength="100" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <button type="reset" id="limpiarFormUsuarios" class="btn btn-info btn-block">Nuevo</button>
                    </div>
                    <div class="form-group col-md-6">
                        <button type="submit" id="submitFormUsuarios" class="btn btn-success btn-block">Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
