<h1><?php echo $varAcceso['nombre']; ?></h1>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <div class="page-header font-weight-lighter">
                (!) Primero seleccionar el usuario y luego guarde el permiso a los accesos
            </div>
            <hr>
            <hr>
            <form id="formUsuarios" role="form" class="form-horizontal">
               <div class="form-group offset-md-1">
                   <label for="usuarios" class="col-md-3 control-label">Usuarios</label>
                   <div class="col-md-9">
                       <select id="usuarios" class="form-control form-control-chosen" required></select>
                   </div>
               </div>
                <div class="form-group">
                    <div class="offset-md-1 col-md-9">
                        <button type="submit" class="btn btn-block btn-info">Consultar</button>
                    </div>
                </div>
            </form>
            <hr>
            <form role="form" class="form-horizontal" id="formGuardarPermisos">
                <div class="form-group offset-md-1">
                    <label for="usuario" class="col-md-3 control-label">Usuario</label>
                    <div class="col-md-9">
                        <input id="usuario" class="form-control" type="text" disabled>
                    </div>
                </div>
                <div class="form-group offset-md-1">
                    <label for="nombre" class="col-md-3 control-label">Nombre</label>
                    <div class="col-md-9">
                        <input id="nombre" class="form-control" type="text" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="offset-md-1 col-md-9">
                        <button type="submit" class="btn btn-block btn-success" id="submitFormGuardarPermisos">Guardar</button>
                        <button type="reset" class="btn btn-block btn-danger" id="limpiarFormGuardarPermisos">Limpiar</button>
                    </div>
                </div>

            </form>
        </div>
        <div id="sys_menu" class="offset-md-2 col-md-9"></div>
    </div>
</div>

