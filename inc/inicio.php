<!-- para mostrar el nombre de la pagina según la variable varAcceso-->
<h1><?php echo $varAcceso['nombre']; ?></h1>
<div class="row">
    <div id="grafico_inicio" class="col-md-12">
        <figure class="highcharts-figure">
            <div id="container"></div>
            <p class="highcharts-description">
                Chart showing data loaded dynamically. The individual data points can
                be clicked to display more information.
            </p>
        </figure>
    </div>
</div>