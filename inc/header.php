<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sistema Base</title>


    <?php
    for($f=0; $f<count($varAcceso['framework']); $f++){
        //print_r($varAcceso);
        switch ($varAcceso['framework'][$f]){
            case 'bootstrap':
                echo '<link rel="stylesheet" href="lib/css/bootstrap.min.css" type="text/css">';
                echo '<link rel="stylesheet" href="lib/css/bootstrap.css" type="text/css">';
                break;
            case 'fontawesome':
                echo '<link rel="stylesheet" href="lib/fontawesome/css/all.css" type="text/css">';
                //echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">';
            break;
            case 'jquery-treeview':
                echo '<link rel="stylesheet" href="lib/js/jquery-treeview-master/jquery.treeview.css" type="text/css">';
                break;
            case 'chosen':
                echo '<link rel="stylesheet" href="lib/css/bootstrap4c-chosen-master/dist/css/component-chosen.css" type="text/css">';
                echo '<link rel="stylesheet" href="lib/css/bootstrap4c-chosen-master/dist/css/component-chosen.min.css" type="text/css">';
                break;
            case 'datatables':
                echo '<link rel="stylesheet" href="lib/js/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css" type="text/css">';
                echo '<link rel="stylesheet" href="lib/js/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.css" type="text/css">';
                echo '<link rel="stylesheet" href="lib/js/DataTables/datatables.css" type="text/css">';
                echo '<link rel="stylesheet" href="lib/js/DataTables/datatables.min.css" type="text/css">';
                break;
        }
    }
    ?>
</head>
<body>
<link rel="stylesheet" href="css/system.css?v=<?php echo $parametros['webversion']?>" type="text/css" >
<link rel="stylesheet" href="css/<?php echo $pagina;?>.css" type="text/css" >
<!--Parametros de la aplicacion-->
<input type="hidden" id="parametro_timout" value="<?php echo $parametros['timeout']?>">

<!-- Modal para advertencias -->
<div class="modal" tabindex="-1" role="dialog" id="myModalWarning">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-warning">
            <div class="modal-header panel-heading">
                <h5 class="modal-title">Advertencia!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center" id="myModalWarningBody">
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Peligro -->
<div class="modal" tabindex="-1" role="dialog" id="myModalDanger">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-danger">
            <div class="modal-header panel-heading">
                <h5 class="modal-title">Peligro!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center" id="myModalDangerBody">
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Success -->
<div class="modal" tabindex="-1" role="dialog" id="myModalSuccess">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-success">
            <div class="modal-header panel-heading">
                <h5 class="modal-title">Éxito!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center" id="myModalSuccessBody">
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light navbar fixed-top">
    <a class="navbar-brand" href="#">Sistema Web</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
<!--            <li class="nav-item active">-->
<!--                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>-->
<!--            </li>-->
            <?php
            $listaMenu = '';
            for($f=0; $f<count($vectorMenu); $f++){
//                for($km=0; $km<count($vectorMenu); $km++){
//                    print_r($vectorMenu[$km]);
//                    print_r('<br>');
//                }
                //verificacion de esMenu
                if($vectorMenu[$f]['esMenu'] == 'SI'){
                    $menuAbierto = '';
                    $listaMenuInterna = '<ul class="dropdown-menu">';

                    for($i=0; $i<count($vectorMenu); $i++){
                        if($vectorMenu[$i]['esMenu'] == 'NO' && $vectorMenu[$i]['idPadre'] == $vectorMenu[$f]['idMenu']){

                            if($pagina == $vectorMenu[$i]['ventana']){
                                $menuAbierto = 'class="active"';
                            }

                            $listaMenuInterna.= '<li '.$menuAbierto.'><i class="'.$vectorMenu[$i]['icono'].'"></i><a href="'.$vectorMenu[$i]['ventana'].'  ">';
                            $listaMenuInterna.= " - ".$vectorMenu[$i]['nombre'];
                            $listaMenuInterna.= '</a></li>';
                        }
                    }
                    $listaMenuInterna .= '</ul>';

                    $listaMenu .= '<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown">';
                    //print_r($vectorMenu[$f]['icono']);
                    $listaMenu .= '<i class = "'.$vectorMenu[$f]['icono'].' "></i> '.$vectorMenu[$f]['nombre'].' <b class="caret"></b>';
                    $listaMenu .= '</a>';
                    $listaMenu .= " ".$listaMenuInterna." ";
                    $listaMenu .= '</li>';
                }
            }
            echo $listaMenu;
            ?>
        </ul>
        <ul class="navbar navbar-nav navbar-right">
            <li>Bienvenido: <b> <?php echo $_SESSION['nombre'] ;?>  </b></li>
            <li> <a href="util/system/logout.php"> <i class="fas fa-sign-out-alt"></i> </a> </li>
<!--            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">-->
<!--            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>-->
        </ul>
    </div>
</nav>
