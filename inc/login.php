<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- boostrap css -->
    <link rel="stylesheet" href="lib/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="lib/css/bootstrap.min.css" type="text/css">
<!--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
    <!-- en el siguiente link va un trick en href, es una peticion get al servidor para ver la webversion
     esa wea es para que no se cacheen los js y los css-->
    <link rel="stylesheet" href="css/login.css?v=<?php echo $parametros['webversion']?>" type="text/css" >
    <title>Sistema Base</title>
</head>
<body>

<!-- Modal para advertencias -->
<div class="modal" tabindex="-1" role="dialog" id="myModalWarning">
    <div class="modal-dialog" role="document">
        <div class="modal-content panel-warning">
            <div class="modal-header panel-heading">
                <h5 class="modal-title">Advertencia</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center" id="myModalWarningBody">
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-md-6 offset-3">
            <div class="page-header text-center">
            <h1>Acceso al Sistema</h1>
            </div>
<!--            <div class="alert alert-info text-center">-->
            <div class="alert alert-info text-center">
                Bienvenido
            </div>
            <div class="card">
                <div class="card-header">
                    Ingrese credenciales
                </div>
                <br>
                <div class="card-body">
                    <form class="form-horizontal" role="form" id="formLogin">
                        <div class="form-group">
                            <label for="usuario" class="col-md-3 control-label">
                                Usuario
                            </label>
                            <div class="col-md-10">
                                <input type="text" id="usuario" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="clave" class="col-md-3 control-label">
                                Contraseña
                            </label>
                            <div class="col-md-10">
                                <input type="password" id="clave" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="offset-2 col-md-10 text-right">
                                <button type="submit" id="btnSubmit" class="btn-info btn">
                                    Ingresar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
    </div>
</div>

<!-- boostrap js -->
<script src="lib/js/jquery-3.4.1.min.js" type="text/javascript"></script>
<script src="lib/js/bootstrap.min.js" type="text/javascript"></script>
<script src="lib/js/bootstrap.js" type="text/javascript"></script>
<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
<!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>-->
<script src="js/login.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="js/login.js?v=<?php echo$parametros['webversion'];?>"> </script>
</body>
</html>