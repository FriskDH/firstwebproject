</div>
<footer class="text-right">
    <hr>
    <p>
        <strong>Desarrollado por "Frisk"</strong>
    </p>
</footer>
</body>
    <?php
    for($f=0; $f<count($varAcceso['framework']); $f++){
        switch ($varAcceso['framework'][$f]){
            case 'bootstrap':
                echo '<script src="lib/css/bootstrap.min.css" type="text/css"></script>';
            break;
            case 'jquery':
                echo '<script src="lib/js/jquery-3.4.1.min.js" type="text/javascript"></script>';
            break;
            case 'jquery-treeview':
                echo '<script src="lib/js/jquery-treeview-master/jquery.treeview.js" type="text/javascript"></script>';
            break;
            //para que no se mire feo chosen, hay que implementar en el header el css del siguiente link,
            //https://github.com/haubek/bootstrap4c-chosen
            case 'chosen':
                //echo '<script src="lib/js/chosen_v1.8.7/chosen.min.css" type="text/css"></script>';
                //echo '<script src="lib/js/chosen_v1.8.7/chosen.css" type="text/css"></script>';
                echo '<script src="lib/js/chosen_v1.8.7/chosen.jquery.min.js" type="text/javascript"></script>';
                echo '<script src="lib/js/chosen_v1.8.7/chosen.jquery.js" type="text/javascript"></script>';
            break;
            case 'highcharts':
                echo '<script src="lib/js/Highcharts/code/highcharts.js" type="text/javascript"></script>';
                echo '<script src="lib/js/Highcharts/code/modules/exporting.js"></script>';
                echo '<script src="lib/js/Highcharts/code/modules/export-data.js"></script>';
                echo '<script src="lib/js/Highcharts/code/modules/accessibility.js"></script>';
                echo '<script src="lib/js/Highcharts/code/modules/series-label.js"></script>';
                echo '<script src="lib/js/Highcharts/code/modules/data.js"></script>';
                break;
            case 'datatables':
                //echo '<script src="lib/js/chosen_v1.8.7/chosen.min.css" type="text/css"></script>';
                //echo '<script src="lib/js/chosen_v1.8.7/chosen.css" type="text/css"></script>';
                echo '<script src="lib/js/DataTables/datatables.js" type="text/javascript"></script>';
                echo '<script src="lib/js/DataTables/datatables.min.js" type="text/javascript"></script>';
                break;
        }
    }
    ?>
    <script type="text/javascript" language="javascript" src="js/system.js?v=<?php echo$parametros['webversion'];?>"> </script>
    <script type="text/javascript" language="javascript" src="js/<?php echo $pagina; ?>.js"> </script>
    <script type="text/javascript" language="JavaScript" src="lib/js/bootstrap.min.js" ></script>
<!--    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<!--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
</html>
