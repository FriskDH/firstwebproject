<?php
//para manejar las sesiones

class Session{
	private $codSession = "sistemaBase";
	
	//el constructor se declara asi:
	public function __construct(){
		session_name($this->codSession);
		session_start();
	}
	
	public function checkSession(){
		//$_SESSION es una variable global
		$check = false;
		if(	isset($_SESSION['usuario']) &&	!empty($_SESSION['usuario'])){
			$check = true;
		}
		
		return $check;
	}
	
	public function createSession(array $datos){
		//se vacía la variable global, ahora contiene un array vacío	
		$_SESSION = array();
		//se asignan los datos de la sesión
		$_SESSION['usuario'] = $datos['usuario'];
		$_SESSION['nombre'] = $datos['nombre']; 
	}
	
	public function endSession(){
		//se vacía la sesión
		$_SESSION = array();
		
		//validación para vaciar las cookies de la sesión
		if(ini_get("session.use_cookies")){
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
		}
		session_destroy();
	}
	
	
}

/*
prueba para comprobar la funcionalidad de las sesiones
 
$sesion = new Session();

if( !$sesion->checkSession() ){
	$sesion->createSession( 
		array('usuario' => 'test',
		'nombre' => 'Yomi :v'		
		) );
}

$sesion->endSession();

print_r($_SESSION);
 */