<?php

//validación para ver si la clase existe
if( !class_exists("Funciones") ){
	//sino que incluya las funciones
	include("funciones.php");
}

class Conexion{
	private $baseDatos, $usuario, $clave, $servidor, $conexion, $puerto, $logs;
	
	public function __construct( $logs ){
		$this->baseDatos = 'sistemabase';
		$this->usuario = 'root';
		$this->clave = 'toorbot123';
		$this->servidor = 'localhost';
		$this->puerto = 3306;
		$this->logs = $logs;
	}
	
	//este metodo sirve por si queremos extraer datos de diferentes servidores...
	public function parametros($baseDatos, $usuario, $clave, $servidor, $puerto = 3306){
		$this->baseDatos = $baseDatos;
		$this->usuario = $usuario;
		$this->clave = $clave;
		$this->servidor = $servidor;
		$this->puerto = $puerto;
	}
	
	public function conectar(){
		//instancia de la coneccion mysqli
		$mysqli = new mysqli($this->servidor, $this->usuario, $this->clave, $this->baseDatos, $this->puerto);
		
		if( $mysqli->connect_error ){
			//si hay error se guarda un log
			Funciones::Logs("ConexionDB", $this->logs, "Error de conexión: ( ".$mysqli->connect_errno." ) ".$mysqli->connect_error);
			//para ver en pantalla que error es
			die("Error de conexión: ( ".$mysqli->connect_errno." ) ".$mysqli->connect_error);
			$this->conexion = false;
			return false;
		}else{
			//sino hay error, se guarda como conexion el objeto mysqli
			$this->conexion = $mysqli;
			$this->conexion->set_charset('utf8');
			return true;
		}
	}
	
	public function ejecutarConsulta( $sql ){
		$resultado = $this->conexion->query($sql);
		
		if($resultado){
			return $resultado;
		}else{
			Funciones::Logs("ConsultaDB", $this->logs, "Error en el query: ( ".$this->conexion->error." ) ".$sql);
			die("Error en el query: ( ".$this->conexion->error." ) ".$sql);
			return false;
		}
	}
	
	//el chiste del destructor es liberar recursos que solicitó el objeto, y es el último método que se autoejecuta
	public function __destruct(){
		if( $this->conexion ){
			$this->conexion->close();
		}
	}
	
}
/*
$conexion = new Conexion('../logs/');
$conexion->conectar();
$resultado = $conexion->ejecutarConsulta("select * from usuarios");
print_r($resultado->num_rows);

if( $resultado->num_rows > 0 ){
    echo "<pre>";
    foreach($resultado as $fila){
        print_r($fila);
    }
    echo "</pre>";
}
*/
