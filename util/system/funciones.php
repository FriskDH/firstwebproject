<?php

class Funciones{
	//para guardar en un txt los logs
	public static function Logs($nombre_archivo, $ubicacion, $descripcion){
		$carpeta = $ubicacion.date('Y').'/'.date('m').'/'.date('d').'/';
		if(!file_exists($ubicacion.date('Y').'/'.date('m').'/'.date('d'))){
			mkdir($ubicacion.date('Y').'/'.date('m').'/'.date('d'), 0777, true);
		}
		
		$archivo = fopen($carpeta.$nombre_archivo.'txt', "a") or die("Archivo inaccesible");
		fwrite($archivo, date('Y-m-d H:i:s').' > '.$descripcion."\r\n");
		fclose($archivo);

	}

	public static function ObtenerIP(){
	    $ipaddress = '';
	    if(getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public static function ObtenerNavegador($userAgent){
	    if(preg_match('|MSIE ([0-9].[0-9]{1,2})|', $userAgent, $matched)){
	        $browserVersion = $matched[1];
	        $browser = 'IE';
        }
	    elseif (preg_match('|Opera/([0-9].[0-9]{1,2})|', $userAgent, $matched)){
            $browserVersion = $matched[1];
	        $browser = 'Opera';
        }
        elseif (preg_match('|Firefox/([0-9\.]+)|', $userAgent, $matched)){
            $browserVersion = $matched[1];
            $browser = 'Firefox';
        }
        elseif (preg_match('|Chrome/([0-9\.]+)|', $userAgent, $matched)){
            $browserVersion = $matched[1];
            $browser = 'Chrome';
        }
        elseif (preg_match('|Safari/([0-9\.]+)|', $userAgent, $matched)){
            $browserVersion = $matched[1];
            $browser = 'Safari';
        }
	    else{
	        //navegador no reconocido
            $browserVersion = 0;
            $browser = 'Desconocido';
        }
	    return $browser." - ".$browserVersion;
    }
}

//Funciones::Logs('testeo', '../logs/', 'ahuevos, si funciona :v');