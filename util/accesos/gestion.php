<?php
//aqui se ejecutan el otorgar o eliminar los permisos del tree view
include ('../system/funciones.php');
include ('../system/sessions.php');
include ('../system/conexion.php');

$conexion = new Conexion('../logs/');
$conexion->conectar();
$session = new Session();
$respuesta = new stdClass();
$respuesta->estado = 1;
$respuesta->mensaje = '';

try{
    if(!$session->checkSession()) throw new Exception('Debe iniciar una sesion');

    $usuario='';
    $menuChckeado= array();

    if (isset($_POST['usuario']) && !empty($_POST['usuario']) ) {
        $usuario = $_POST['usuario'];
    }

    if (isset($_POST['menuCheckeado'])) {
        $menuChckeado = $_POST['menuCheckeado'];
    }

    $resultadoDelete= $conexion->ejecutarConsulta(
        "DELETE FROM usuarios_accesos WHERE usuario='".$usuario."'"
    );
    if($resultadoDelete != true){
        throw new Exception("Error al realizar la eliminacion");
    }

    if(empty($resultadoDelete)){
        throw new Exception("El usuario esta vacio");
    }

    if(count($menuChckeado) > 0){
        for($i=0; $i< count($menuChckeado); $i++){
            $conexion->ejecutarConsulta("
            INSERT INTO usuarios_accesos(idMenu, usuario, usuario_creacion, fecha_creacion)
            VALUES ('".$menuChckeado[$i]."', '".$usuario."', '".$_SESSION['usuario']."', NOW());
            ");
        }

        $respuesta->mensaje= "Accesos otorgados";
    }else{
        $respuesta->estado= 2;
        $respuesta->mensaje= "Accesos eliminados";
    }


}catch(Exception $e){
    $respuesta->estado = 2;
    $respuesta->mensaje = $e->getMessage();
}
print_r(json_encode($respuesta));
