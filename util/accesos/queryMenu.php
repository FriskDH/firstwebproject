<?php
include ('../system/funciones.php');
include ('../system/sessions.php');
include ('../system/conexion.php');

$conexion = new Conexion('../logs/');
$conexion->conectar();
$session = new Session();
$respuesta = new stdClass();
$respuesta->estado = 1;
$respuesta->mensaje = '';
$respuesta->data = array();

try{
    if(!$session->checkSession()) throw new Exception('Debe iniciar una sesion');

    //resultado del query para extraer todas las filas de los usuarios y su asignacion en $respuesta->data como un array
    $resultado = $conexion->ejecutarConsulta('SELECT idMenu, nombre FROM menu WHERE esMenu="SI" and estado="ACTIVO"
    ORDER BY orden');
    $contador = 0;
    foreach ($resultado as $fila){
            $respuesta->data[$contador]['idMenu'] = $fila['idMenu'];
            $respuesta->data[$contador]['nombre'] = $fila['nombre'];
            //como cada menu puede tener submenus, se hace lo siguiente
            $respuesta->data[$contador]['subMenu'] = array();

            $resultadoSubMenu = $conexion->ejecutarConsulta('SELECT idMenu, nombre FROM menu WHERE idPadre="'.$fila['idMenu'].'" and estado="ACTIVO"
            ORDER BY orden');
            $contadorInterno = 0;

            foreach($resultadoSubMenu as $filaInterna){
                //ojo
                $respuesta->data[$contador]['subMenu'][$contadorInterno]['idMenu'] = $filaInterna['idMenu'];
                $respuesta->data[$contador]['subMenu'][$contadorInterno]['nombre'] = $filaInterna['nombre'];
                $contadorInterno++;
            }
            $contador++;
    }

}catch(Exception $e){
    $respuesta->estado = 2;
    $respuesta->mensaje = $e->getMessage();
}
print_r(json_encode($respuesta));