<?php
include ('../system/funciones.php');
include ('../system/sessions.php');
include ('../system/conexion.php');

$conexion = new Conexion('../logs/');
$conexion->conectar();
$session = new Session();
$respuesta = new stdClass();
$respuesta->estado = 1;
$respuesta->mensaje = '';
$respuesta->data = array();

try{
    if(!$session->checkSession()) throw new Exception('Debe iniciar una sesion');

    $usuario = '';

    //la variable no va a llegar por el metodo post
    if(isset($_POST['usuario']) && !empty($_POST['usuario'])){
        $usuario = $_POST['usuario'];
    }

    if(empty($usuario)){
        throw new Exception("El usuario esta vacio");
    }

    $resultado = $conexion->ejecutarConsulta("
    SELECT idMenu FROM usuarios_accesos WHERE usuario='".$usuario."'");

    foreach ($resultado as $fila){
        $respuesta->data[]=$fila['idMenu'];
    }

}catch(Exception $e){
    $respuesta->estado = 2;
    $respuesta->mensaje = $e->getMessage();
}
print_r(json_encode($respuesta));