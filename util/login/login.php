<?php
include('../system/funciones.php');
include('../system/sessions.php');
include('../system/conexion.php');

$conexion = new Conexion("../logs/");
$conexion->conectar();
$session = new Session();
//se crea una clase predefinida
$respuesta = new stdClass();
$respuesta->estado = 1;
$respuesta->mensaje = "EFE";

//obtención y validación de datos del form login
try{
    $usuario = "";
    $clave = "";
    if( (isset($_POST['usuario']) && !empty($_POST['usuario']) ) &&
        (isset($_POST['clave']) && !empty($_POST['clave'])) ){

        $usuario = $_POST['usuario'];
        $clave = $_POST['clave'];

    }

    if( empty($usuario) || empty($clave) ){
        throw new Exception("Usuario o clave vacías");
    }

    //comprobacion de datos
    $datosUsuario = array();
    $claveCifrada = hash("sha512", "sistemita.Jeje123".$clave);
    $resultado = $conexion->ejecutarConsulta(
        "select * from usuarios where usuario = '".$usuario."'
        and clave = '".$claveCifrada."' LIMIT 1");

    foreach ($resultado as $fila){
        //array_push($datosUsuario, $fila);
        $datosUsuario = $fila;
    }

//    $datosUsuario['nombre'];
//    $datosUsuario['email'];
//    $datosUsuario['tipo_usuario'];

    //si el vector de datos esta vacio:
    if( count($datosUsuario) == 0 ){
        throw new Exception("Los datos ingresados no están registrados");
    }

    $session->createSession($datosUsuario);
    //print_r("Inicio exitoso");

}catch(Exception $e){
    $respuesta->estado = 2;
    $respuesta->mensaje = $e->getMessage();
}
sleep(2.5);
print_r(json_encode($respuesta));