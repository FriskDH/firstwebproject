<?php

//////////////////
/*
 * lo que se hace aquí es preguntarle al sistema: oye tengo acceso a esta pagina?
 * sino entonces traeme a las que si
 * sino tiene acceso a ninguna, entonces gg :v
 */
//////////////////

include("util/system/funciones.php");
include("util/system/sessions.php");
include("util/system/conexion.php");

$conexion = new Conexion("util/logs/");
$conexion->conectar();
$session = new Session();


//obtener los parametros del sistema
////////////////////////////////////////
$resultadoParametros = $conexion->ejecutarConsulta("select * from parametros");
$parametros = array();
foreach($resultadoParametros as $fila){
    $parametros[trim($fila['parametro'])] = trim($fila['valor']);
}


//si hay una sesion abierta, incluimos los modulos del sistema
if( $session->checkSession() ){

    //validación del tiempo de sesión
    if( isset($_SESSION['fechaSesion'] ) ){
        $fechaGuardar = $_SESSION['fechaSesion'];
        $ahora = date('Y-m-d H:i:s');
        $tiempoTranscurrido = (strtotime($ahora) - strtotime($fechaGuardar));
        if( $tiempoTranscurrido >= ($parametros['timeout'])*60){
            $session->endSession();
            header("Refresh:0");
            exit();
        }else{
            $_SESSION['fechaSesion'] = date('Y-m-d H:i:s');
        }

    }else{
        $_SESSION['fechaSesion'] = date('Y-m-d H:i:s');
    }

    //conseguir el(los) modulos que tiene asignado el usuario en sesión actual
    $pagina = $parametros['paginadefecto'];
    //url por defecto
    if( isset($_GET['pagina']) && !empty($_GET['pagina']) ){
        $pagina = $_GET['pagina'];
    }

    //traer los permisos de la pagina para el usuario
    $resultadoPermisos = $conexion->ejecutarConsulta("
    select a.idMenu, b.ventana, b.framework, b.nombre
    from usuarios_accesos as a
    inner join menu as b on (a.idMenu = b.idMenu)
    where a.usuario = '".$_SESSION['usuario']."'
    and b.ventana = '".$pagina."'
    and b.estado = 'ACTIVO'
    ");

    $varAcceso = array();
    foreach ($resultadoPermisos as $fila){
        $varAcceso['idMenu'] = $fila['idMenu'];
        $varAcceso['nombre'] = $fila['nombre'];
        $varAcceso['ventana'] = $fila['ventana'];
        $varAcceso['framework'] = explode(",",$fila['framework']);
    }

    //en caso de que el usuario no tenga permisos, se verifica en cual menu lo tiene
    if( count($varAcceso) == 0 ){
        $flagAccesoPagina = false;
        //query para ver permisos, orden de jerarquía por permisos
        $resultadoAccesoPagina = $conexion->ejecutarConsulta("
        select a.ventana
        from menu as a
        inner join usuarios_accesos as b on (a.idMenu = b.idMenu)
        where b.usuario = '".$_SESSION['usuario']."'
        and a.estado = 'ACTIVO'
        and a.esMenu = 'NO'
        order by a.idPadre, a.orden
        limit 1
        ");

        foreach ($resultadoAccesoPagina as $fila){
            $pagina = $fila['ventana'];
            $flagAccesoPagina = true;
        }

        if($flagAccesoPagina == false){
            $session->endSession();
            echo "El usuario que ha ingresado no tiene asignados módulos en el sistema, contactarse con el
            administrador del sistema";
            header("Refresh:10");
            exit();
        }else{
            //en caso de que si tenga paginas asignadas, se vuelve a hacer lo mismo
            $resultadoVerificacion = $conexion->ejecutarConsulta("
            select a.idMenu, b.ventana, b.framework 
            from usuarios_accesos as a
            inner join menu as b on (a.idMenu = b.idMenu)
            where a.usuario = '".$_SESSION['usuario']."'
            and b.ventana = '".$pagina."'
            and b.estado = 'ACTIVO'
            ");

            $varAcceso = array();
            foreach ($resultadoVerificacion as $fila){
                $varAcceso['idMenu'] = $fila['idMenu'];
                $varAcceso['nombre'] = $fila['nombre'];
                $varAcceso['ventana'] = $fila['ventana'];
                $varAcceso['framework'] = explode(",",$fila['framework']);
            }

        }
    }

    if( count($varAcceso) > 0){
        $conexion->ejecutarConsulta("
        INSERT INTO log_menu (ip, navegador, usuario, idMenu, nombre, ventana, fecha) 
        VALUES ('".Funciones::ObtenerIP()."','".Funciones::ObtenerNavegador($_SERVER['HTTP_USER_AGENT'])."'
        ,'".$_SESSION['usuario']."',
        ".$varAcceso['idMenu'].",'".$varAcceso['nombre']."','".$varAcceso['ventana']."',NOW())
        ");

    }

    //obtener el menú según jerarquías
    //en esta consulta se obtienen los que realmente son menús, menús completos
    $resultadoMenu = $conexion->ejecutarConsulta("
    select a.idMenu, a.nombre, a.idPadre, a.ventana, a.esMenu, a.icono
    from menu as a
    left join usuarios_accesos as b on (a.idMenu = b.idMenu)
    where  b.usuario = '".$_SESSION['usuario']."'
    and a.estado = 'ACTIVO'
    and a.idPadre IS NOT NULL
    order by a.orden
    ");

    $vectorMenu = array();
    $conVectorMenu = 0;
    foreach ($resultadoMenu as $fila){
        $vectorMenu[$conVectorMenu]['idMenu'] = $fila['idMenu'];
        $vectorMenu[$conVectorMenu]['nombre'] = $fila['nombre'];
        $vectorMenu[$conVectorMenu]['idPadre'] = $fila['idPadre'];
        $vectorMenu[$conVectorMenu]['ventana'] = $fila['ventana'];
        $vectorMenu[$conVectorMenu]['esMenu'] = $fila['esMenu'];
        $vectorMenu[$conVectorMenu]['icono'] = $fila['icono'];
        $conVectorMenu++;
    }

    $idPadreIn = '';
    for($i = 0; $i<count($vectorMenu); $i++){
        if($i == 0){
            $idPadreIn .= $vectorMenu[$i]['idPadre'];
        }else{
            $idPadreIn .= ','.$vectorMenu[$i]['idPadre'];
        }
    }

    if(!empty($idPadreIn)){
        //este query es para extraer todos los menus padres
        $resultadoMenuPadre = $conexion->ejecutarConsulta("
        select a.idMenu, a.nombre, a.idPadre, a.ventana, a.esMenu, a.icono
        from menu as a
        where a.idMenu in (".$idPadreIn.")
        and a.estado = 'ACTIVO'
        order by a.orden
        ");
    }

    foreach ($resultadoMenuPadre as $fila){
        $vectorMenu[$conVectorMenu]['idMenu'] = $fila['idMenu'];
        $vectorMenu[$conVectorMenu]['nombre'] = $fila['nombre'];
        $vectorMenu[$conVectorMenu]['idPadre'] = $fila['idPadre'];
        $vectorMenu[$conVectorMenu]['ventana'] = $fila['ventana'];
        $vectorMenu[$conVectorMenu]['esMenu'] = $fila['esMenu'];
        $vectorMenu[$conVectorMenu]['icono'] = $fila['icono'];
        $conVectorMenu++;
    }

    //al final, se extraen primero los submenus y luego se extraen los datos de los mismos submenus

//    echo "<pre>";
//    print_r($vectorMenu);
//    echo "</pre>";

    //inclusion de los archivos
    include("inc/header.php");
    include('inc/'.$pagina.'.php');
    include("inc/footer.php");

}else{
    //sino hay una sesión iniciada, se muestra el login
    include("inc/login.php");
}